module gitlab.com/alonelegion/building_microservices/product-api

go 1.14

require (
	github.com/go-playground/validator/v10 v10.3.0
	github.com/gorilla/mux v1.7.4
)
