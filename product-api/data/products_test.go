package data

import "testing"

func TestChecksVlidation(t *testing.T) {
	p := &Product{
		Name:  "andy",
		Price: 1.00,
		SKU:   "abs-abs-def",
	}

	err := p.Validate()

	if err != nil {
		t.Fatal(err)
	}
}
